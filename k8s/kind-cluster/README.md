# Create and configure a kind cluster

For this PoC you can create and configure your k8s cluster with this _README_ config file and then run this command from the same directory.
```
kind create cluster --config=config.yaml
```


Create : _TODO_ reorganize it
1. SetUp Ingress nginx controller
2. A metallb namespace
3. A memberlist secrets
4. Apply metallb manifest
5. Wainting for metallb pods to have a status Running 
6. Setup address pool used by loadbalancers

## Ingress nginx Controller (step 1)
Complete config.yaml to allow extraPortMappings and node-labels. This operation allow the local host to make requests to the Ingress controller over ports 80 and 443. node-labels only allow ingres controller to run a specific node matching the label selector.

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
kubectl wait --namespace ingress-nginx   --for=condition=ready pod   --selector=app.kubernetes.io/component=controller   --timeout=180s
```

## Installing a LoadBalancer (step 2 to 5)


```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/namespace.yaml
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)" 
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/metallb.yaml
kubectl get pods -n metallb-system --watch

```


## Setup address pool used by loadbalancers (step 6)
Complete layer2 configuration. You need to provide a range of ip adddresses controlled by metallb. Youn can display this range on the docker kind network.
Use this command if you want this range:
```
docker network inspect -f '{{.IPAM.Config}}' kind
```
The output will contain a cidr. Use this cidr on our loadbalancer IP range. Complete and use the metallb-configmap.yaml to setup metallb loadbalancer range.
```
kubectl apply -f metallb-configmap.yaml
```