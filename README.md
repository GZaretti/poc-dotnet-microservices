# Getting started

1. /k8s/kind-cluster/README.md
2. GitOps deployments
3. Microservices

# Introduction
This is a PoC devloped during my home lab. In this repository you can find a microservice architecture in .Net with two microservices and this Kubernetes deployments. 

# Architecture overview
This architecture is inspired by Les Jackson Binarythistle. 
This PoC is a very simplified system for handles platforms and commands microservices. This project has for goal an approach and an overview of microservices architecture. Building two microservices with Data Layer (Database Object and Data Transfert Object), Repository, Controller, Actions, HTTP Client. After you install a cluster Kubernetes with Kind with a Ingresse controller and a LoadBalancer. Containerizing, publishing to Docker Hub and create a declarative deployment for Kubernetes. Adding an API Gateway on this PoC.


![alt text](docs/schemas/Kubernetes architecture.png)