
using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using PlatformService.Dtos;

namespace PlateformService.SyncDataServices.Http{

    public class HttpCommandDataClient : ICommandDataClient
    {
        private readonly HttpClient htttpClient;
        private readonly IConfiguration configuration;

        public HttpCommandDataClient(HttpClient httpClient, IConfiguration configuration)
        {
            this.htttpClient = httpClient;
            this.configuration = configuration;
        }

        public async Task SendPlatformToCommand(PlatformReadDto platform)
        {
            var httpContent = new StringContent(
                JsonSerializer.Serialize(platform), Encoding.UTF8,
                "application/json"
            );

            var response = await this.htttpClient.PostAsync($"{this.configuration["CommandService"]}/api/c/platforms/",httpContent);

            if(response.IsSuccessStatusCode)
            {
                Console.WriteLine("--> sync POST to CommandService was OK!");
            }
            else
            {
                Console.WriteLine("--> sync POST to CommandService was NOT OK!");
            }
        }
    }
}